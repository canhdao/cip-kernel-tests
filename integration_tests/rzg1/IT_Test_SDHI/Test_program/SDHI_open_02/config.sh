#!/bin/sh
#Function: Detect MMC/SDHI information

get_last_blks () {
	# echo "get last blocks of $1"
	local logical_blksize=''
	local total_parts=''
	local total_bytes=''
	local blk_number=''

	# check if device is available
	if [ ! -e "/dev/${1}" ]
	then
		# echo "device not found!"
		echo "N/A"
		return
	fi

	# get logical block size
	logical_blksize=`lsblk -t /dev/${1} | sed -n 2p | awk '{print $6}'`

	# get last 4 blocks number
	## check:
	## if device has no partition, get blocks of disk
	## if device has partitions, get blocks of last's partition
	total_parts=`lsblk -b /dev/${1} | egrep "part" -c`

	if [ $total_parts -eq 0 ]
	then
		total_bytes=`lsblk -b /dev/${1} | egrep "disk" | awk '{sum_bytes+=$4} END {printf "%.0f\n", sum_bytes}'`
	elif [ $total_parts -gt 0 ]
	then
		total_bytes=`lsblk -b /dev/${1} | egrep "part" | awk '{sum_bytes+=$4} END {printf "%.0f\n", sum_bytes}'`
	fi

	# echo "total_bytes $total_bytes"
	# echo "logical_blksize $logical_blksize"
	blk_number=`expr $total_bytes / $logical_blksize - 4`
	echo -e "$logical_blksize\t $blk_number"
}

if [ -f "board_config.txt" ]
then
	rm -rf board_config.txt
	touch board_config.txt
else
	touch board_config.txt
fi

#mmc_check=`dmesg | egrep -w "mmc0:|mmc1:|mmc2:" | egrep "] mmc" | awk -F'] ' '{print $NF}' | egrep MMC | awk '{print $1}' | sed -e 's/://g'`
#mmc_check=`dmesg | egrep -w "mmcblk0|mmcblk1|mmcblk2"  | egrep -v boot | egrep MMC | awk '{print $1}' | sed -e 's/://g'`
mmc_check=`dmesg | egrep -w "mmcblk0|mmcblk1|mmcblk2"  | egrep -v boot | egrep "MMC|004GE0" | sed -e 's/.*mmcblk0/mmcblk0/g' -e 's/.*mmcblk1/mmcblk1/g' -e 's/.*mmcblk2/mmcblk2/g' | awk '{print $1}' | sed -e 's/://g'`
if [ $mmc_check == "mmcblk0" ]
then
	mmc="mmcblk0"
	sd0="mmcblk1"
	sd1="mmcblk2"
fi


if [ $mmc_check == "mmcblk1" ]
then
	mmc="mmcblk1"
	sd0="mmcblk0"
	sd1="mmcblk2"
fi


if [ $mmc_check == "mmcblk2" ]
then
	mmc="mmcblk2"
	sd0="mmcblk0"
	sd1="mmcblk1"
fi


if [ $mmc_check == "" ]
then
	sd0="mmcblk0"
	sd1="mmcblk1"
	mmc="dont_exist"
fi

sd0_count=`ls /dev/mmc* | egrep -w $sd0 | wc -l`
sd1_count=`ls /dev/mmc* | egrep -w $sd1 | wc -l`

if [ $sd0_count != "1" ]
then
	sd0="dont_exist"
fi

if [ $sd1_count != "1" ]
then
	sd1="dont_exist"
fi

mmc_blk_info=`get_last_blks $mmc`
sd0_blk_info=`get_last_blks $sd0`
sd1_blk_info=`get_last_blks $sd1`

echo -e "MMC_DEV = $mmc\t $mmc_blk_info" >> board_config.txt
echo -e "SD0_DEV = $sd0\t $sd0_blk_info" >> board_config.txt
echo -e "SD1_DEV = $sd1\t $sd1_blk_info" >> board_config.txt
