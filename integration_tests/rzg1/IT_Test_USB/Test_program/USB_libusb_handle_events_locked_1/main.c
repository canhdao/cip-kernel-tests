/*
 * Project: RZG_IT_CIP_BSP
 * Test ID: test_case
 * Feature: Checking libusb_handle_events_locked system call
 * Sequence: libusb_init();libusb_lock_events();libusb_unlock_events();libusb_exit();
 * Testing level: system call
 * Test-case type: Normal
 * Expected result: OK
 * Name: main.c
 * Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
 * Version: v00r01
 * Copyright (C) 2019  Renesas Electronics Corporation
 * Target board: G1M_G1E_G1C_G1N_G1H
 * Details_description: External device: USB Storage. Condition: Call API libusb_handle_events_locked of usblib library. Expected result = OK
 */
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <libusb.h>
#include <sys/types.h>
#include <stdlib.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main(int argc, char *argv[])
{
	int result;
	struct libusb_context *ctx;

	struct timeval timeout;
	timeout.tv_sec  = 0;       // seconds
	timeout.tv_usec = 100000;  // ( .1 sec)

	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	//Main checking item
	libusb_init(&ctx);
	if (result < 0) {
		printf("Error: libusb_init");
		return 0;
	}
	//This implementation uses a blocking call and aquires a lock to the event handler
	libusb_lock_events(ctx);
	result = libusb_handle_events_locked(ctx, &timeout); // Test function with timeout 0.1 sec
	if(result < 0)					     // Expected value 0
		goto Exit;
	libusb_unlock_events(ctx);

Exit:
	libusb_exit(ctx);
	if( result == 0 ) {
		printf ("OK\n");
	} else {
		printf ("NG\n");
	}

	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}


